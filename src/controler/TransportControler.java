package controler;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import model.DataLoader;
import model.Subscriber;
import model.Path;
import model.Stop;
import model.Vehicle;
import model.VehicleType;
import utility.Arranger;
import view.BadgingDialog;
import view.ChangingStopDialog;
import view.MainFrame;
import view.TextFrame;

public class TransportControler implements Runnable, Observer {
	MainFrame frame;
	TextFrame textFrame;
	private Thread animator;
	protected ArrayList<Path> paths = new ArrayList<Path>();
	double period;
	protected boolean suspended = false;
	

	
	public TransportControler() {
		period = 10.;
		textFrame = new TextFrame();
		frame = new MainFrame(this);

				
		DataLoader dataLoader = new DataLoader("data.xml", this);
		
		for (Stop s : dataLoader.getStops()) {
			Stop.getAllStops().add(s);
			frame.addStop(s);
		}
		
		paths = dataLoader.getPaths();
		
		
		int n = 0;
		
		for (Subscriber s : dataLoader.getSubscribers()) {
			Subscriber.getAllSubscribers().add(s);
			frame.addLocatedObject(s);
			n++;
		}
		
		if (n > 0) frame.setEnabledBadging(true);
	
		
		for (Path path : paths) {
			frame.addPath(path);
			
			textFrame.println("Ligne : " + path.getPathName());
			textFrame.println("\tArr�ts :");
			for (Stop s : path.getStops()) {
				String spaces = "                          \t";
				if (s.getStopName().length() < spaces.length()) spaces = spaces.substring(s.getStopName().length());
				textFrame.println(
					"\t\t" + s.getStopName() + 
					spaces + "(" + (int)s.getX() + ", " + (int)s.getY() + ")"
				);
			}
			
			textFrame.println("\n\tV�hicules :");
			for (Vehicle v : path.getVehicles()) {
				String vType = "";
				switch(v.getVehicleType()) {
				case BUS: vType = DataLoader.VEHICLE_TYPE_BUS; break;
				case CAR: vType = DataLoader.VEHICLE_TYPE_CAR; break;
				case MOTO: vType = DataLoader.VEHICLE_TYPE_MOTO; break;
				}
				textFrame.println(
					"\t\t" + v.getName() + " (" + vType + ")    (" + 
					path.getStopById(v.getMyStationId()) + ")"
				);
				frame.addVehicle(v);
			}
			textFrame.println("-----------------------------------------\n");
		}
		textFrame.println("\n__________________________________________________________________");
		
		
		textFrame.setVisible(true);
		frame.setVisible(true);
			
		this.start();
	}
	

	
	public void start() {
		if (animator == null) {
			animator = new Thread (this);
			animator.start ();
		}
	}
	

	
	public void badge(Subscriber subscriber, Stop currentStop, Stop targetStop, VehicleType preferredVehicleType) {
		ArrayList<Path> paths = Stop.getPathsContaining2Stops(currentStop, targetStop); 
		
		subscriber.setCurrentStop(currentStop);
		
		if (subscriber.getVehicle() == null) {
			if (paths.size() > 0) {
				subscriber.setTargetStop(targetStop);
				subscriber.setVisible(true);
				subscriber.setPosition(currentStop.getPosition());
				subscriber.setWaiting(true);
				currentStop.addSubscriber(subscriber);
				subscriber.setPreferredVehicleType(preferredVehicleType);
				
				Arranger<Vehicle> availableVehicles = new Arranger<Vehicle>();
				
				for (Path p : paths) {
					for (Vehicle v : p.getVehicles()) {
						if (v.getVehicleType() == preferredVehicleType) {
							int value = v.calculateDistanceToStop(currentStop);
							value += p.calculateDistanceBetween(currentStop, targetStop);
	
							if (v.canAddSubscriberToQueueForStops(currentStop, targetStop)) availableVehicles.add(v, value);
						}
					}
				}
							
				for (Vehicle v : availableVehicles.getObjects()) {
					if ( v.addSubscriberToQueue(subscriber) ) break;
				}
			} 
			else {
				System.out.println(
						"Erreur pour " + subscriber.getName() 
						+ ", il n'y a pas de ligne qui contient les deux arr�ts : " 
						+ currentStop.getStopName() + " et " + targetStop.getStopName()
				);
			}
		}
	}
	
	public void badge(int subscriberIndex, int currentStopIndex, int targetStopIndex, int preferredVehicleTypeIndex) {
		Subscriber subsc = Subscriber.getAllSubscribers().get(subscriberIndex);
		Stop currentStop = Stop.getAllStops().get(currentStopIndex);
		Stop targetStop = Stop.getAllStops().get(targetStopIndex);
		VehicleType vt = null;
		
		subsc.setPosition(currentStop.getPosition());
		
		switch (preferredVehicleTypeIndex) {
		case 0:
			vt = VehicleType.BUS;
			break;
		case 1:
			vt = VehicleType.CAR;
			break;
		case 2:
			vt = VehicleType.MOTO;
			break;
		}
		
		badge(subsc, currentStop, targetStop, vt);
	}
	
	public void addNewSubscriber(String name) {
		ArrayList<Subscriber> subscribers = Subscriber.getAllSubscribers();
		Subscriber s = new Subscriber(name, subscribers.size() * 100, subscribers.size() * 100);
		subscribers.add(s);
		frame.addLocatedObject(s);

		frame.setEnabledBadging(true);
	}
	
	
	
	public Arranger<String> getSubscribersNotInAnyVehicle() {
		return Subscriber.getSubscribersNotInAnyVehicle();
	}	
	
	public Arranger<String> getSubscribersInVehicles() {
		return Subscriber.getSubscribersInVehicles();
	}
	
	public Arranger<String> getStopsBetweenSubscriberAndTarget(int subscriberIndex) {
		Arranger<String> res = new Arranger<String>();
		
		Subscriber subscriber = Subscriber.getAllSubscribers().get(subscriberIndex);
		
		if (subscriber.getVehicle() != null) {
			Path path = subscriber.getVehicle().getPath();
			for (Stop s : path.getStopsBetween(subscriber.getVehicle().getNearTargetStop(), subscriber.getTargetStop())) {
				res.add(s.getStopName(), Stop.getAllStops().indexOf(s));
			}
		}
		
		return res;
	}
	
	public void setSubscriberTargetStopIndex(int subscriber, int targetStop) {
		Subscriber.getAllSubscribers().get(subscriber).setTargetStop(Stop.getAllStops().get(targetStop));
	}
	
	public ArrayList<VehicleType> getAvailableVehicleTypes(Stop currentStop, Stop targetStop) {
		ArrayList<VehicleType> res = new ArrayList<VehicleType>();
		ArrayList<Path> paths = Stop.getPathsContaining2Stops(currentStop, targetStop);
		
		for (Path p : paths) {
			for (Vehicle v : p.getVehicles()) {
				if (v.canAddSubscriberToQueueForStops(currentStop, targetStop)) {
					if ( ! res.contains(v.getVehicleType()) ) res.add(v.getVehicleType());
				}
			}
		}
		
		return res;
	}
	
	public String[] getAvailableVehicleTypes(int currentStopIndex, int targetStopIndex) {
		String res[];
		
		ArrayList<VehicleType> vehicleTypes = getAvailableVehicleTypes(
				Stop.getAllStops().get(currentStopIndex), 
				Stop.getAllStops().get(targetStopIndex)
		);
		
		res = new String[vehicleTypes.size()];
		
		for (int i = 0; i < vehicleTypes.size(); i++) res[i] = vehicleTypes.get(i).toString();
		
		return res;
	}
	
	public Arranger<String> getIndexAndNamesOfAvailableVehicleTypes(int currentStopIndex, int targetStopIndex) {
		Arranger<String> res = new Arranger<String>();
		
		ArrayList<VehicleType> vehicleTypes = getAvailableVehicleTypes(
				Stop.getAllStops().get(currentStopIndex), 
				Stop.getAllStops().get(targetStopIndex)
		);
				
		for (int i = 0; i < vehicleTypes.size(); i++) {
			int idx = 0;
			String name = null;
			
			switch (vehicleTypes.get(i)) {
			case BUS:
				idx = 0;
				name = "Bus";
				break;
			case CAR:
				idx = 1;
				name = "Voiture";
				break;
			case MOTO:
				idx = 2;
				name = "Moto";
				break;
			default:
				break;
			}
			res.add(name, idx);
		}
		
		return res;
	}
	
	public static Arranger<String> getNamesOfLinkedStopsWithIndexedStop(int stopIndex) {
		return Stop.getNamesOfLinkedStopsWithIndexedStop(stopIndex);
	}
	
	public static String[] getAllStopsNames() {
		return Stop.getAllStopsNames();
	}


	public boolean showBadgingDialog() {
		if (Subscriber.getSubscribersNotInAnyVehicle().size() > 0) {
			BadgingDialog f = new BadgingDialog(this, frame);
			f.setVisible(true);
		}
		else return false;
		
		return true;
	}
	
	public boolean showBadgingDialog(int x, int y, int currentStopIndex) {
		if (Subscriber.getSubscribersNotInAnyVehicle().size() > 0) {
			BadgingDialog f = new BadgingDialog(this, frame, x, y, currentStopIndex);
			f.setVisible(true);
		}
		else return false;
		
		return true;
	}
	
	public boolean showChangingStopDialog() {
		if (Subscriber.getSubscribersInVehicles().size() > 0) {
			ChangingStopDialog f = new ChangingStopDialog(this, frame);
			f.setVisible(true);
		}
		else return false; 
		
		return true;
	}
	
	public void increasePeriod() {
		period *= 1.2;
	}
	
	public void decreasePeriod() {
		period /= 1.2;
		if (period < 1) period = 1;
	}
	
	@Override
	public void run() {
		while (animator != null) {
			try {
				Thread.sleep((int)period);

				for (Path p : paths) p.moveVehicles();
				for (Subscriber s : Subscriber.getAllSubscribers()) s.move();
				
				frame.repaint();
				synchronized(this){
					while(suspended) wait();
				}
			} catch (Exception e) {
				e.printStackTrace ();
			}
		}	
	}
	
	public void suspend() {
		suspended = true;
	}
	
	public synchronized void resume() {
		suspended = false;
		notify();
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		
		if (arg0.getClass() == Vehicle.class) textFrame.addText((String)arg1 + "\n");

		frame.setEnabledBadging(Subscriber.getSubscribersNotInAnyVehicle().size() > 0);
		frame.setEnabledChangeStop(Subscriber.getSubscribersInVehicles().size() > 0); 
	}
}
