package model;

import java.util.ArrayList;
import java.util.Observer;

import utility.AttributeXMLFileReader;

public class DataLoader {
	final static String PATH = "ligne";
	final static String STOP = "arret";
	final static String VEHICLE = "vehicule";
	final static String SUBSCRIBER = "abonne";
	
	public final static String VEHICLE_TYPE_BUS = "bus";
	public final static String VEHICLE_TYPE_CAR = "voiture";
	public final static String VEHICLE_TYPE_MOTO = "moto";
	
	ArrayList<String> stopIds       = new ArrayList<String>();
	ArrayList<String> pathIds       = new ArrayList<String>();
	ArrayList<String> vehicleIds    = new ArrayList<String>();
	ArrayList<String> subscriberIds = new ArrayList<String>();
	
	ArrayList<Stop>       stops       = new ArrayList<Stop>();
	ArrayList<Path>       paths       = new ArrayList<Path>();
	ArrayList<Vehicle>    vehicles    = new ArrayList<Vehicle>();
	ArrayList<Subscriber> subscribers = new ArrayList<Subscriber>();
	
	public DataLoader (String fileName, Observer o) {
		AttributeXMLFileReader reader1, reader2;
		
		reader1 = new AttributeXMLFileReader(fileName, STOP); 
		reader1.addAttribute("name");
		reader1.addAttribute("x");
		reader1.addAttribute("y");
		reader1.addAttribute("id");
		for (ArrayList<String> row1 : reader1.getData()) {
			
			// si stop n'existe pas encore dans stops
			if ( stopIds.indexOf(row1.get(3)) == -1 
			&& row1.get(0) != "" && row1.get(1) != "" && row1.get(2) != "") {
				stopIds.add(row1.get(3));
				Stop stop = new Stop(row1.get(0), Double.parseDouble(row1.get(1)), Double.parseDouble(row1.get(2)), row1.get(3));
				stops.add(stop);
				
				
				reader2 = new AttributeXMLFileReader(fileName, VEHICLE); 
				reader2.addAttribute("id");
				reader2.addAttribute("type");
				reader2.addAttribute("name");
				for (ArrayList<String> row2 : reader2.getDataWhoseParentIs(STOP, row1.get(3))) {
					if ( vehicleIds.indexOf(row2.get(0)) == -1 ) {
						vehicleIds.add(row2.get(0));
						
						VehicleType vehicleType = null;
						
						switch (row2.get(1)) {
						case VEHICLE_TYPE_BUS: vehicleType = VehicleType.BUS; break;
						case VEHICLE_TYPE_CAR: vehicleType = VehicleType.CAR; break;
						case VEHICLE_TYPE_MOTO: vehicleType = VehicleType.MOTO; break;
						}
						
						Vehicle vehicle = new Vehicle(row2.get(0), vehicleType, row2.get(2), o);
						
						vehicle.setMyStationId(row1.get(3));
						vehicles.add(vehicle);
					}
				}
			}
			else { // si stop existe d�j� dans stops
				Stop stop = stops.get(stopIds.indexOf( row1.get(3) ));
				
				if ( row1.get(0) != "" ) stop.setStopName( row1.get(0) );
				if ( row1.get(1) != "" ) stop.setX( Double.parseDouble(row1.get(1)) );
				if ( row1.get(2) != "" ) stop.setY( Double.parseDouble(row1.get(2)) );
			}
		}

	
		reader1 = new AttributeXMLFileReader(fileName, PATH); 
		reader1.addAttribute("name");
		reader1.addAttribute("id");
		for (ArrayList<String> row : reader1.getData()) {
			if ( pathIds.indexOf(row.get(1)) == -1 ) {  // si path n'existe pas encore dans paths
				
				pathIds.add(row.get(1));
				
				Path path = new Path(row.get(0), row.get(1));
				paths.add(path);
				
				
				reader2 = new AttributeXMLFileReader(fileName, STOP); 
				reader2.addAttribute("name");
				reader2.addAttribute("x");
				reader2.addAttribute("y");
				reader2.addAttribute("id");
				for (ArrayList<String> row2 : reader2.getDataWhoseParentIs(PATH, row.get(1))) {
					if ( stopIds.indexOf(row2.get(3)) == -1) {  // si stop n'existe pas encore dans stops
						if (row2.get(0) != "" && row2.get(1) != "" && row2.get(2) != "") {
							stopIds.add(row2.get(3));
							Stop stop = new Stop(row2.get(0), Double.parseDouble(row2.get(1)), Double.parseDouble(row2.get(2)), row2.get(3));
							stops.add(stop);
							path.addStop(stop);
						}

					}
					else {
						path.addStop( stops.get(stopIds.indexOf(row2.get(3))) );
					}
				}

			
				reader2 = new AttributeXMLFileReader(fileName, VEHICLE); 
				reader2.addAttribute("id");
				reader2.addAttribute("type");
				reader2.addAttribute("name");
				for (ArrayList<String> row2 : reader2.getDataWhoseParentIs(PATH, row.get(1))) {
					if ( vehicleIds.indexOf(row2.get(0)) == -1 ) {
						vehicleIds.add(row2.get(0));
						
						VehicleType vehicleType = null;
						
						switch (row2.get(1)) {
						case VEHICLE_TYPE_BUS: vehicleType = VehicleType.BUS; break;
						case VEHICLE_TYPE_CAR: vehicleType = VehicleType.CAR; break;
						case VEHICLE_TYPE_MOTO: vehicleType = VehicleType.MOTO; break;
						}
						
						Vehicle vehicle = new Vehicle(row2.get(0), vehicleType, row2.get(2), o);

						vehicles.add(vehicle);
						path.addVehicle(vehicle);
					}
					else
					{
						path.addVehicle( vehicles.get(vehicleIds.indexOf(row2.get(0))) );
					}
				}
			}
		}
		
		reader1 = new AttributeXMLFileReader(fileName, SUBSCRIBER); 
		reader1.addAttribute("name");
		reader1.addAttribute("x");
		reader1.addAttribute("y");
		reader1.addAttribute("id");
		reader1.addAttribute("visible");
		for (ArrayList<String> row1 : reader1.getData()) {
			if ( subscriberIds.indexOf(row1.get(3)) == -1 ) {
				Subscriber subscriber = new Subscriber();
				String name, id;
				Double x = 0., y = 0.;
				boolean visible = false;
				
				name = row1.get(0);
				if ( row1.get(1) != "" ) x = Double.parseDouble( row1.get(1) );
				if ( row1.get(2) != "" ) y = Double.parseDouble( row1.get(2) );
				id = row1.get(3);
				if ( row1.get(4).equals("true") ) visible = true;
				
				subscriber.setName(name);
				subscriber.setId(id);
				subscriber.setX(x);
				subscriber.setY(y);
				subscriber.setVisible(visible);

				subscriberIds.add(id);
				subscribers.add(subscriber);
			}
		}
	}
	
	

	public ArrayList<Stop> getStops() {
		return stops ;
	}
	
	public ArrayList<Path> getPaths() {
		return paths;
	}
	
	public ArrayList<Vehicle> getVehicles() {
		return vehicles;
	}
	
	public ArrayList<Subscriber> getSubscribers() {
		return subscribers;
	}
}
