package model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;

import utility.Drawable;

public class Path extends Drawable {
	protected ArrayList<Stop> stops;
	protected ArrayList<Vehicle> vehicles;
	private boolean visible;
	private String pathName;
	private String id;
	
	public Path() {
		initialize();
	}
	
	public Path(String name) {
		pathName = name;
		initialize();
	}
	
	public Path(String name, String id) {
		pathName = name;
		this.id = id;
		initialize();
	}
	
	private void initialize() {
		stops = new ArrayList<Stop>();
		vehicles = new ArrayList<Vehicle>();
		setVisible(true);
	}
	
	public void addStop(Stop stop) {
		if (stop != null) {
			stops.add(stop);		
			stop.addPath(this);
		}
	}
	
	public void addVehicle(Vehicle vehicle) {
		vehicles.add(vehicle);
		vehicle.setPath(this);
		
		if (stops.size() > 0){
			Stop stop = stops.get(0);
			if (vehicle.getMyStationId() == null) vehicle.setMyStationId(stop.getId());
			if ( this.getStopById(vehicle.getMyStationId()) == null ) vehicle.setMyStationId( stop.getId() );
			
			//vehicle.setPosition( this.getStopById(vehicle.getMyStationId()).getPosition() );
			stop = this.getStopById(vehicle.getMyStationId());

			vehicle.setTargetStop(stop);
			vehicle.setStarted(true);
		}
	}
	
	public Stop getNextStop(Stop stop) {
		int i = stops.indexOf(stop);		
		return stops.get( (i+1) % stops.size() );
	}
	
	public Stop getStopById(String id) {
		for (Stop s : stops) if (s.getId().equals(id)) return s;
		return null;
	}
	
	
	public void moveVehicles() {
		for (Vehicle v : vehicles) {
			v.move();
		}
	}
	
	public ArrayList<Stop> getStops() {
		return stops;
	}
	
	public Vehicle getVehicle(int index) {
		return vehicles.get(index);
	}
	
	public ArrayList<Vehicle> getVehicles() {
		return vehicles;
	}

	public String getPathName() {
		return pathName;
	}

	public void setPathName(String pathName) {
		this.pathName = pathName;
	}

	public boolean containsStop(Stop stop) {
		return stops.contains(stop);
	}
	
	public boolean containsVehicleOfType(VehicleType vehicleType) {
		for (Vehicle v : vehicles) {
			if (v.vehicleType == vehicleType) return true;
		}
		return false;
	}
	
	public int calculateDistanceBetween(Stop beginStop, Stop destinationStop) {
		int res = 1000;
		
		if (stops.contains(beginStop) && stops.contains(destinationStop)) {
			res = stops.indexOf(destinationStop) - stops.indexOf(beginStop);
			if (res < 0) res += stops.size();
		}
		
		return res;
	}
	
	public ArrayList<Stop> getStopsBetween(Stop stop1, Stop stop2) {
		ArrayList<Stop> res = new ArrayList<Stop>();
		
		for (Stop s : this.getStops()) {
			int d1 = this.calculateDistanceBetween(stop1, stop2);
			int d2 = this.calculateDistanceBetween(stop1, s);
			
			if (d2 >= 0 && d2 <= d1) {
				res.add(s);
			}
		}
		
		return res;
	}
	
	public String toString() {
		return this.getPathName();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	public void draw(Graphics g) {
		ArrayList<Point> points = new ArrayList<Point>();
		int x = 0, y = 0, count = 0;
		
		if (this.isVisible()) {
			ArrayList<Stop> stops = this.getStops();
			if (stops.size() > 0) {
				g.setColor(Color.blue);
				
				for (Stop s : stops) {
					x = (int)s.getX();
					y = (int)s.getY();
					if (count > 0) g.drawLine(x, y, points.get(count-1).x, points.get(count-1).y);
					points.add(new Point(x, y));
					count++;
				}
				
				g.drawLine(x, y, points.get(0).x, points.get(0).y);
			}
		}
	}
}
