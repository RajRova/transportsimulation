package model;

import java.awt.Toolkit;
import java.util.ArrayList;

import utility.Arranger;
import utility.LocatedObject;

public class Stop extends LocatedObject {
	protected static ArrayList<Stop> allStops = new ArrayList<Stop>();
	protected ArrayList<Path> paths;
	private String stopName;
	private ArrayList<Subscriber> subscribers = new ArrayList<Subscriber>(); 
	private String id;
	
	public Stop() {
		initialize();
	}
	
	public Stop(String name) {
		initialize();
		this.setStopName(name);
	}

	public Stop(String name, double x, double y) {
		initialize();
		this.setStopName(name);
		setPosition(x, y);
	}

	public Stop(String name, double x, double y, String id) {
		initialize();
		this.setStopName(name);
		this.id = id;
		setPosition(x, y);
	}
	
	public Stop(double x, double y) {
		initialize();
		setPosition(x, y);
	}
	
	private void initialize() {
		allStops.add(this);
		this.setImageSize(90, 90);
		
		paths = new ArrayList<Path>();
		
		setVisible(true);
		this.setImage(Toolkit.getDefaultToolkit().getImage("images/Bus_Stop.jpg"));
	}
	
//	public static Stop[] getAllStops() {
//		Stop stops[] = new Stop[allStops.size()];
//		for (int i = 0; i < allStops.size(); i++) stops[i] = allStops.get(i);
//		return stops;
//	}

	public static ArrayList<Stop> getAllStops() {
		return allStops;
	}
	
	public static Stop getStopById(String id) {
		for (Stop s : Stop.getAllStops()) if (s.getId().equals(id)) return s;
		return null;
	}
	
	public static String[] getAllStopsNames() {
		int n = allStops.size();
		String res[] = new String[n];
		for (int i = 0; i < n; i++) res[i] = allStops.get(i).getStopName();
		return res;
	}
	
	public static Arranger<String> getNamesOfLinkedStopsWithIndexedStop(int index) {
		Arranger<String> res = new Arranger<String>();
		
		ArrayList<Stop> linkedStops = Stop.getLinkedStopsWith(allStops.get(index));
		int n = linkedStops.size();
		
		for (int i = 0; i < n; i++) {
			res.add(linkedStops.get(i).getStopName(), allStops.indexOf(linkedStops.get(i)));
		}
		
		return res;
	}
	
	//Ceci permet d'obtenir la liste de toutes les lignes contenant les deux stops s1 et s2
	public static ArrayList<Path> getPathsContaining2Stops(Stop s1, Stop s2) {
		ArrayList<Path> res = new ArrayList<Path>();
		for (Path p : s1.getPaths()) {
			if (s2.getPaths().contains(p)) res.add(p);
		}
		return res;
	}
	
	

	
	public ArrayList<Path> getPaths() {
		return paths;
	}
	
	public String[] getPathNames() {
		if (paths.size() == 0) return null;

		String names[] = new String[paths.size()];
		for (Path p : paths) names[paths.indexOf(p)] = p.getPathName();

		return names;
	}
	
	protected void addPath(Path path) {
		paths.add(path);
	}

	public String getStopName() {
		return stopName;
	}

	public void setStopName(String stopName) {
		this.setLabelText(stopName);
		this.stopName = stopName;
	}

	public ArrayList<Subscriber> getSubscribers() {
		return subscribers;
	}

	public void addSubscriber(Subscriber subscriber) {
		this.subscribers.add(subscriber);
	}
	
	public void removeSubscriber(Subscriber subscriber) {
		this.subscribers.remove(subscriber);
	}
	
	public ArrayList<Stop> getLinkedStops() {
		return Stop.getLinkedStopsWith(this);
	}
	
	public static ArrayList<Stop> getLinkedStopsWith(Stop stop) {
		ArrayList<Stop> res = new ArrayList<Stop>();
		
		for (Path p : stop.getPaths()) {
			for (Stop s : p.getStops()) {
				if ( ! res.contains(s) ) res.add(s);
			}
		}
		res.remove(stop);
		
		return res;
	}
	
//	public static Stop getStopById(String id) {
//		for (Stop s : Stop.getAllStops()) if (s.getId() == id) return s;
//		return null;
//	}

	
	@Override
	public String toString() {
		return this.getStopName();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}