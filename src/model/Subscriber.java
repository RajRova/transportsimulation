package model;

import java.awt.Toolkit;
import java.util.ArrayList;

import utility.Arranger;
import utility.LocatedObject;
import utility.Randomizer;

public class Subscriber extends LocatedObject {
	private static ArrayList<Subscriber> allSubscribers = new ArrayList<Subscriber>();
	private String name;
	private Stop currentStop, targetStop;
	private VehicleType preferredVehicleType;
	private Vehicle vehicle;  // le v�hicule qui porte le passager
	private String id;
	private boolean waiting;

	public Subscriber() {
		initialize();
	}
	
	public Subscriber(String name) {
		initialize();
		setName(name);
	}	
	
	public Subscriber(String name, double x, double y) {
		initialize();
		this.setPosition(x, y);
		setName(name);
	}

	public Subscriber(String name, double x, double y, String id) {
		initialize();
		setId(id);
		this.setPosition(x, y);
		setName(name);
	}
	
	private void initialize() {
		setVisible(false);
		setWaiting(false);
		setSpeed(.05);
		
		this.setImageSize(40, 60);
		int i = Randomizer.getRandomIntegerBetween(1, 6);
		
		if (i < 4) this.setImage(Toolkit.getDefaultToolkit().getImage("images/man" + i + ".gif"));
		else this.setImage(Toolkit.getDefaultToolkit().getImage("images/woman" + (i-3) + ".png"));
	}
	

	
	public static ArrayList<Subscriber> getAllSubscribers() {
		return allSubscribers;
	}
	
	public static String[] getSubscribersNames() {
		int n = Subscriber.getAllSubscribers().size();
		String res[] = new String[n];
		for (int i = 0; i < n; i++) res[i] = Subscriber.getAllSubscribers().get(i).getName();
		return res;
	}
	
	public static Arranger<String> getSubscribersNotInAnyVehicle() {
		Arranger<String> res = new Arranger<String>(); 
		ArrayList<Subscriber> subscribers = Subscriber.getAllSubscribers();
		for (int i = 0; i < subscribers.size(); i++) {
			if (subscribers.get(i).getVehicle() == null) res.add(subscribers.get(i).getName(), i);
		}
		return res;
	}

	public static Arranger<String> getSubscribersInVehicles() {
		Arranger<String> res = new Arranger<String>(); 
		ArrayList<Subscriber> subscribers = Subscriber.getAllSubscribers();
		for (int i = 0; i < subscribers.size(); i++) {
			if (subscribers.get(i).getVehicle() != null) res.add(subscribers.get(i).getName(), i);
		}
		return res;
	}

	
	public Stop getCurrentStop() {
		return currentStop;
	}
	
	public void setCurrentStop(Stop value) {
		currentStop = value;
	}
	
	public Stop getTargetStop() {
		return targetStop;
	}
	
	public void setTargetStop(Stop value) {
		targetStop = value;
	}
	
	public void setPreferredVehicleType(VehicleType preferredVehicleType) {
		this.preferredVehicleType = preferredVehicleType;
	}
	
	public VehicleType getPreferredVehicleType() {
		return this.preferredVehicleType;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return getName();
	}
	


	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isWaiting() {
		return waiting;
	}

	public void setWaiting(boolean waiting) {
		this.waiting = waiting;
	}

	public void move() {
		if(!isWaiting()) {
			if (0 == Randomizer.getRandomIntegerBetween(0, 500)){
				setTargetPosition(Randomizer.getRandomIntegerBetween(0, 500), Randomizer.getRandomIntegerBetween(0, 500));
			}
			super.moveToTarget();
		}
	}
}
