package model;

import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Observer;

import utility.LocatedObject;
import utility.MyVector;
import utility.Randomizer;

public class Vehicle extends LocatedObject {
	protected VehicleType vehicleType;
	protected String name;
	protected boolean started;
	protected Stop nearTargetStop;
	protected Stop farTargetStop;  // l'arr�t de d�stination du v�hicule
	protected Path path;
	private int nbPlaces;
	private ArrayList<Subscriber> passengers; // les voyageurs dans le v�hicule
	private ArrayList<Subscriber> subscriberQueue;  // les abonn�s � prendre
	private String Id;
	private String myStationId;
	
	
	
	public Vehicle() {
		setType(VehicleType.BUS);
		initialize();
	}
	
	public Vehicle(VehicleType vehicleType) {
		setType(vehicleType);
		initialize();
	}
	
	public Vehicle(VehicleType vehicleType, String name) {
		setType(vehicleType);
		setName(name);
		initialize();
	}	
	
	public Vehicle(VehicleType vehicleType, String name, Observer o) {
		setType(vehicleType);
		setName(name);
		addObserver(o);
		initialize();
	}	
	
	public Vehicle(String id, VehicleType vehicleType, String name, Observer o) {
		setType(vehicleType);
		setId(id);
		setName(name);
		addObserver(o);
		initialize();
	}
	
	
	
	public void setType(VehicleType vehicleType) {
		if (vehicleType == null) {setType(VehicleType.BUS); return;}
		this.vehicleType = vehicleType;
		switch (vehicleType) {
		case BUS:
			setSpeed(.5);
			setNbPlaces(22);
			this.setImageSize(80, 70);
			this.setImage(Toolkit.getDefaultToolkit().getImage("images/Bus.png"));
			break;
		case CAR:
			setSpeed(.7);
			setNbPlaces(4);
			this.setImageSize(60, 45);
			this.setImage(Toolkit.getDefaultToolkit().getImage("images/taxi.gif"));
			break;
		case MOTO:
			setSpeed(.9);
			setNbPlaces(1);
			this.setImageSize(50, 40);
			this.setImage(Toolkit.getDefaultToolkit().getImage("images/moto.gif"));
			break;
		default:
			setType(VehicleType.BUS);
			break;
		}
	}
	
	public VehicleType getVehicleType() {
		return vehicleType;
	}
	
	private void initialize() {
		setVisible(true);
		started = false;
		passengers = new ArrayList<Subscriber>();
		subscriberQueue = new ArrayList<Subscriber>();
		setPosition(Randomizer.getRandomIntegerBetween(100, 500), Randomizer.getRandomIntegerBetween(100, 500));
	}

	
	
	
	public Path getPath() {
		return this.path;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setTargetStop(Stop stop) {
		setTargetPosition(stop.getX(), stop.getY());
		setNearTargetStop(stop);
	}
	
	public Stop getNearTargetStop() {
		return nearTargetStop;
	}
	
	public void setNearTargetStop(Stop stop) {
		nearTargetStop = stop;
	}
	
	
	
	/* Ceci sert � indiquer "vehicle" � aller vers un "stop" en suivant le chemin selon le ligne "path"
	et il faut ajouter ce "vehicle" dans la liste de "path" avant d'appeler cette m�thode,
	c'est-�-dire utiliser addVehicle(...) avant    */
	public void setFarTargetStop(Stop stop) {
		farTargetStop = stop;
		setTargetPosition( this.getNearTargetStop().getPosition() );
		setStarted(true);
	}	
	
	public void setFarTargetStop(String stopId) {
		farTargetStop = path.getStopById(stopId);
		setFarTargetStop(farTargetStop); 
	}
	
	
	/* m�me chose que setFarTargetStop(Stop stop);
	 * il faut ajouter ce "vehicle" dans la liste de "path" avant d'appeler cette m�thode,
	   c'est-�-dire utiliser addVehicle(...) avant     */
	public void setFarTargetStopIndex(int index) {
		if (path == null) {
			System.out.println("Erreur lors de l'utilisation de setFarTargetStopIndex(int) ce V�hicule n'a pas encore de \"path\" utilisez d'abord path.addVehicle(vehicle)");
		}
		if (index > 0 && index < path.stops.size())	{
			farTargetStop = path.stops.get(index);
		}
		setTargetPosition( this.getNearTargetStop().getPosition() );
		setStarted(true);
	}
	
	

	
	public void moveToTarget() {
		if (started) {
			super.moveToTarget();			
			for(Subscriber s : this.getPassengers()) s.setPosition(this.getX(), this.getY());
		}
	}
	
	public void move() {
		moveToTarget();
		
		if (targetReached() && started) {

			
			Stop stopReached = this.getNearTargetStop();
			
			String sentenceIntro = "";
			
			switch ( this.getVehicleType() ) {
			case BUS: sentenceIntro = "Le bus ";
				break;
			case CAR: sentenceIntro = "La voiture ";
				break;
			case MOTO: sentenceIntro = "Le moto ";
				break;
			}
			
			this.embarkSubscribersInStop(stopReached);
			this.disembarkSubscribersToStop(stopReached);

			this.setChanged();
			this.notifyObservers(sentenceIntro + this + " arrive � l'arr�t " + stopReached);

			
			if (farTargetStop != null) {
				if (getX() == farTargetStop.getX() && getY() == farTargetStop.getY()) { 
					setStarted(false);	
				}
			}
			
			if (started) {
				Stop stop = path.getNextStop(getNearTargetStop());
				setTargetStop(stop);
			}
		}
	}
	
	//Cette fonction retourne true si le v�hicule a atteint son cible, donc s'il est sur un arr�t
	public boolean targetReached() {
		if ( getX() == targetX && getY() == targetY) return true;
		else if (new MyVector(getX()-targetX, getY()-targetY).getMagnitude() <= this.speed);
		return false;
	}
	
	public boolean embarkSubscribersInStop(Stop stop) {
		boolean res = false;
		

		for (Subscriber subscriberInQueue : stop.getSubscribers()) {
			if (this.getSubscriberQueue().contains(subscriberInQueue) && this.getPassengers().size() < this.getNbPlaces()) {

				if (subscriberInQueue.getCurrentStop() == stop) {
					System.out.println(subscriberInQueue.getName() + " monte depuis l'arr�t " + stop.getStopName());
					
					this.setChanged();

					String type = "";
					switch (this.vehicleType) {
					case BUS: type = "un bus"; break;
					case CAR: type = "une voiture"; break;
					case MOTO: type = "un moto"; break;
					}
					
					this.notifyObservers(
							subscriberInQueue.getName() + 
							" monte dans " + type + " � l'arr�t \"" + 
							stop.getStopName() + "\" dans la ligne \"" + 
							this.path.getPathName() + "\""
					);
					
					subscriberInQueue.setVisible(false);
					subscriberInQueue.setCurrentStop(null);
					this.addPassenger(subscriberInQueue);
					subscriberInQueue.setVehicle(this);
					this.removeSubscriberFromQueue(subscriberInQueue);
					this.setFarTargetStop(subscriberInQueue.getTargetStop());
					
					res = true;
				}
			}
		}
		
		return res;
	}
	
	public void disembarkSubscribersToStop(Stop stop) {
		ArrayList<Subscriber> subscribersToBeRemoved = new ArrayList<Subscriber>();
		
		for (Subscriber s : this.getPassengers()) {
			if (s.getTargetStop() == stop) {
				System.out.println(s.getName() + " arrive � l'arr�t " + stop.getStopName());
				
				String type = "";
				switch (this.vehicleType) {
				case BUS: type = "d'un bus"; break;
				case CAR: type = "d'une voiture"; break;
				case MOTO: type = "d'un moto"; break;
				}
				
				this.setChanged();
				this.notifyObservers(
						s.getName() + " descend " + 
						type + ", arriv� � l'arr�t \"" + 
						stop.getStopName() + "\" dans la ligne \"" + 
						this.path.getPathName() + "\""
				);
				
				s.setVisible(true);
				s.setWaiting(false);
				subscribersToBeRemoved.add(s);
			}
		}
		
		for (Subscriber s : subscribersToBeRemoved) {
			s.setVehicle(null);
			s.setTargetStop(null);
			s.setCurrentStop(null);
			this.getPassengers().remove(s);
		}

		if ( this.getSubscriberQueue().size() == 0 && this.getPassengers().size() == 0 ) {
			this.setFarTargetStop(this.myStationId);
		}
		
	}
	

	
	public void setStarted(boolean value) {
		started = value;
	}

	public void setPath(Path path) {
		this.path = path;
	}

	public int getNbPlaces() {
		return nbPlaces;
	}

	public void setNbPlaces(int nbPlaces) {
		this.nbPlaces = nbPlaces;
	}

	public ArrayList<Subscriber> getPassengers() {
		return passengers;
	}

	public void addPassenger(Subscriber subscriber) {
		this.passengers.add(subscriber);
	}
	
	public void removePassenger(Subscriber subscriber) {
		this.passengers.remove(subscriber);
	}

	public ArrayList<Subscriber> getSubscriberQueue() {
		return subscriberQueue;
	}

	public boolean addSubscriberToQueue(Subscriber subscriber) {
		boolean res = false;
		
		if (this.canAddSubscriberToQueueForStops(subscriber.getCurrentStop(), subscriber.getTargetStop())) {
			this.subscriberQueue.add(subscriber);
						
			if ( farTargetStop == null ) farTargetStop = getNearTargetStop();
			
			if (path.calculateDistanceBetween(getNearTargetStop(), farTargetStop)
			<= path.calculateDistanceBetween(getNearTargetStop(), subscriber.getCurrentStop()))
				this.setFarTargetStop(subscriber.getCurrentStop());

			res = true;
		}
		
		return res;
	}
	
	public boolean canAddSubscriberToQueueForStops(Stop currentStop, Stop targetStop) {
		boolean res = false;
		
		if (this.path.containsStop(currentStop)
		&& this.path.containsStop(targetStop)) {
			if ( nbPlacesSupposedToBeFreeAtStop(currentStop) > 0 ) {
				res = true;
			}
		}
		
		return res;
	}
	
	public void removeSubscriberFromQueue(Subscriber subscriber) {
		this.subscriberQueue.remove(subscriber);
	}
	
	public int nbPlacesSupposedToBeFreeAtStop(Stop stop) {
		int res = getNbPlaces() - getSubscriberQueue().size() + getPassengers().size();
		int d = path.calculateDistanceBetween(getNearTargetStop(), stop);
		
		for (Subscriber s : getSubscriberQueue())
			if (path.calculateDistanceBetween(getNearTargetStop(), s.getTargetStop()) <= d) res++;

		for (Subscriber s : getPassengers())
			if (path.calculateDistanceBetween(getNearTargetStop(), s.getTargetStop()) <= d) res++;

		return res;
	}
	
	public int calculateDistanceToStop(Stop stop) {
		int res = 0;
		
		res = path.calculateDistanceBetween(getNearTargetStop(), stop);
		
		return res;
	}
	
	@Override
	public String toString() {
		return this.getName();
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getMyStationId() {
		return myStationId;
	}

	public void setMyStationId(String stopId) {
		this.myStationId = stopId;
	}
}
