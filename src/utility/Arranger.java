package utility;

import java.util.ArrayList;

public class Arranger<T> {
	ArrayList<MyItem> items = new ArrayList<MyItem>();
	
	public Arranger() {
	}
	
	public void add(T o, int value) {
		int i = 0;
		if (items.size() > 0) 
			for (i = 0; i < items.size(); i++)
				if (items.get(i).getValue() > value) break;
		
		items.add(i, new MyItem(o, value));
	}
	
	public T get(int index) {
		return items.get(index).getObject();
	}
	
	public int getValue(int index) {
		return items.get(index).getValue();
	}
	
	public T getFirstHavingValue(int value) {
		T res = null;
		int i = 0;
		
		while ( i < items.size() && items.get(i).getValue() != value) i++;
		res = items.get(i).getObject();
		
		return res;
	}
	
	public int getFirstIndexHavingValue(int value) {
		int i = 0;
		
		while ( i < items.size() && items.get(i).getValue() != value) i++;
		
		return i;
	}
	
	public T getLeast() {
		T res = null;
		if (items.size() > 0) res = items.get(0).getObject();
		return res;
	}
	
	public ArrayList<T> getObjects() {
		ArrayList<T> objects = new ArrayList<T>();
		for (int i = 0; i < items.size(); i++) {
			objects.add(get(i));
		}
		return objects;
	}
	
	public T getGreater() {
		T res = null;
		if (items.size() > 0) res = items.get(items.size() - 1).getObject();
		return res;
	}
	
	private class MyItem {
		int value;
		T o;
		
		MyItem(T o, int value) {
			this.value = value;
			this.o = o;
		}
		
		int getValue() {return value;}
		T getObject() {return o;}
	}
	
	public int size() {
		return items.size();
	}
	
	@Override
	public String toString() {
		String s = "[";
		for (int i = 0; i < items.size(); i++) {
			s += "[" + items.get(i).getValue() + ", " + items.get(i).getObject() + "]";
		}
		return s + "]";
	}
}
