package utility;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class AttributeXMLFileReader {
	private String fileName;
	private String elementName;
	protected ArrayList<String> attributes = new ArrayList<String>();
	
	public AttributeXMLFileReader(String fileName, String elementName) {
		this.fileName = fileName;
		this.elementName = elementName;
	}
	
	public void addAttribute(String attribute) {
		attributes.add(attribute);
	}
	
	
	// Ceci lit les attributs des �l�ments <elementName ></elementName>
	public ArrayList<ArrayList<String>> getData() {
		ArrayList<ArrayList<String>> data = new ArrayList<ArrayList<String>>(); 
		
		try {
			File fXmlFile = new File(fileName);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName(elementName);


			
			for (int temp = 0; temp < nList.getLength(); temp++) {
				ArrayList<String> row = new ArrayList<String>();
				Node nNode = nList.item(temp);
				
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					
					for (String attr : attributes) {
						row.add(eElement.getAttribute(attr));
					}
				}
				
				data.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return data;
	}
	
	// Ceci lit les attributs des �l�ments <elementName></elementName> inclu dans <parentTagName id=parentId></parentTagName>
	public ArrayList<ArrayList<String>> getDataWhoseParentIs(String parentTagName, String parentId) {
		ArrayList<ArrayList<String>> data = new ArrayList<ArrayList<String>>(); 
		
		try {
			File fXmlFile = new File(fileName);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			doc.getDocumentElement().normalize();

			
			NodeList parentNodeList = doc.getElementsByTagName(parentTagName);
			
			for (int i = 0; i < parentNodeList.getLength(); i++) {
				Node parentItemNode = parentNodeList.item(i);
				
				if (parentItemNode.getNodeType() == Node.ELEMENT_NODE) {
					Element parentElement = (Element) parentItemNode;
					

					if ( parentElement.getAttribute("id").equals(parentId) ) {
						NodeList nList = parentElement.getElementsByTagName(elementName);
						
						for (int temp = 0; temp < nList.getLength(); temp++) {
							ArrayList<String> row = new ArrayList<String>();
							Node nNode = nList.item(temp);
							
							
							
							if (nNode.getNodeType() == Node.ELEMENT_NODE) {
								Element eElement = (Element) nNode;
								
								for (String attr : attributes) {
									row.add(eElement.getAttribute(attr));
								}
							}
							
							data.add(row);
						}
						
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return data;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
