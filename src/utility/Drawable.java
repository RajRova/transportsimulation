package utility;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Observable;

import model.Stop;

public abstract class Drawable extends Observable {	
	protected Image image;
	protected Integer imageWidth;
	protected Integer imageHeight;
	private boolean visible;
	private Point imagePosition;
	protected String labelText;
	protected Rectangle boundingRectangle;
	
	public Drawable() {
		this.setImagePosition(new Point(0, 0));
	}
	
	public void draw(Graphics g) {
		Drawable lo = this;
		if (lo.isVisible()) {
			int x, y;

			x = lo.getImagePosition().x;
			y = lo.getImagePosition().y;
			
			if (lo.getImageWidth() == 0 || lo.getImageHeight() == 0)
				g.drawImage(lo.getImage(), x, y, null);
			else {
				g.drawImage(lo.getImage(),	x, y, lo.getImageWidth(), lo.getImageHeight(), null);
			}
		}
		
		if (getLabelText() != null && getLabelText() != "") {
			Stop s = (Stop)lo;
			g.setColor(Color.black);
			g.drawString(s.getLabelText(), this.getImagePosition().x, getImagePosition().y + 10);
		}

	}
	
	private Image getImage() {
		// TODO Auto-generated method stub
		return image;
	}

	public void setLabelText(String text) {
		this.labelText = text;
	}
	
	public String getLabelText() {
		return labelText;
	}

	public void setImageSize(int width, int height) {
		imageWidth = width;
		imageHeight = height;
	}
	
	public int getImageWidth() {
		return imageWidth;
	}

	public int getImageHeight() {
		return imageHeight;
	}

	
	public boolean isVisible() {
		return visible;
	}
	
	public void setVisible(boolean b) {
		visible = b;
	}

	public Point getImagePosition() {
		return imagePosition;
	}

	public void setImagePosition(Point position) {
		imagePosition = position;
	}
	
	public boolean imageContainsPoint(int x, int y) {
		return boundingRectangle.contains(x, y);
	}
}
