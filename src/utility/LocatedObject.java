package utility;

import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;

public abstract class LocatedObject extends Drawable {
	protected Integer leftImageOffset;
	protected Integer topImageOffset;
	protected double targetX;
	protected double targetY;
	protected double speed;
	private double x, y;
	
	public LocatedObject () {
		leftImageOffset = 0;
		topImageOffset = 0;
		boundingRectangle = new Rectangle();
	}

	public Point getPosition() {
		return new Point((int)x, (int)y);
	}
	
	public double getX() { return x; }
	public double getY() { return y; }
	
	public void setX(double x) { this.x = x; setPosition(this.x, this.y); }
	public void setY(double y) { this.y = y; setPosition(this.x, this.y); }
	public void setX(int x)    { this.x = x; setPosition(this.x, this.y); }
	public void setY(int y)    { this.y = y; setPosition(this.x, this.y); }

	public void setPosition(Point position) {
		setPosition(position.getX(), position.getY());
	}
	
	public void setPosition(double x, double y) {
		setImagePosition(new Point((int)x + leftImageOffset, (int)y + topImageOffset));
		this.x = x;
		this.y = y;
		boundingRectangle.setBounds(
				(int)x + leftImageOffset, 
				(int)y + topImageOffset, 
				imageWidth, 
				imageHeight
		);
	}

	
	public Image getImage() {
		return image;
	}

	protected void setImage(Image im) {
		image = im;
		if (imageWidth == null)	imageWidth = im.getWidth(null);
		if (imageHeight == null) imageHeight = im.getHeight(null);
	}
	
	public void setImageSize(int width, int height) {
		super.setImageSize(width, height);
		leftImageOffset = - width / 2;
		topImageOffset = - height / 2;
		setImagePosition(new Point((int)x + leftImageOffset, (int)y + topImageOffset));
	}
	
	public int getLeftImageOffset() {
		return leftImageOffset;
	}
	
	public int getTopImageOffset() {
		return topImageOffset;
	}
	
	public void setTargetPosition(double x, double y) {
		targetX = x;
		targetY = y;
		if (this.getPosition() == null) this.setPosition(0, 0);
	}
	
	public void setTargetPosition(Point p) {
		setTargetPosition(p.getX(), p.getY());
	}
	
	public void moveToTarget() {
		double x = 0, y = 0;

		MyVector v = new MyVector(
				targetX - this.getX(), 
				targetY - this.getY()
		);
		
		if (speed < v.getMagnitude()) {
			MyVector velocity = v.getUnitVector().productWithScalar(speed);
			
			x = getX() + velocity.getX();
			y = getY() + velocity.getY();
		}
		else {
			x = targetX;
			y = targetY;
		}

		this.setPosition(x, y);
	}
	
	public void setSpeed(double value) {
		speed = value;
	}
}
