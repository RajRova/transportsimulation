package utility;

public class MyVector {
	protected double x;
	protected double y;
	
	public MyVector() {
		
	}
	
	public MyVector(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public MyVector(MyVector vect) {
		this.x = vect.getX();
		this.y = vect.getY();
	}
	
	public void setValues(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public double getMagnitude() {
		return Math.sqrt(x*x + y*y);
	}
	
	public double getX(){
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public static MyVector getUnitVector(MyVector vect) {
		double x, y, m;
		x = vect.getX();
		y = vect.getY();
		m = vect.getMagnitude();
		if (m == 0) {
			x = 0;
			y = 0;
		}
		else {
			x = x / m;
			y = y / m;
		}
		return new MyVector(x, y);
	}
	
	public MyVector getUnitVector() {
		return getUnitVector(this);
	}
	
	public String toString() {
		return x + ", " + y;
	}
	
	public MyVector productWithScalar(double a) {
		MyVector v = new MyVector(this);
		v.setValues(a * v.getX(), a * v.getY());
		return v;
	}
	
	public static double scalarProduct(MyVector v1, MyVector v2) {
		return v1.getX() * v2.getX() + v1.getY() * v2.getY();
	}
}
