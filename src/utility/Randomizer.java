package utility;

import java.util.Random;

public class Randomizer {
	public static int getRandomIntegerBetween(int a, int b) {
		Random rn = new Random();
		return rn.nextInt(Math.abs(b) + Math.abs(a)) % (b + 1 - a) + a;
	}
}
