package view;

import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import controler.TransportControler;

public class AddSubscriberDialog extends MyAbstractDialog {
	private static final long serialVersionUID = 1L;
	protected TextField txtName = new TextField(8);

	public AddSubscriberDialog(TransportControler ctl, Frame owner) {
		super(ctl, "Ajout d'abonn�", owner);

		
		this.setLocation(500, 300);
		center.add(new Label("Nom de l'abonn� : "));
		center.add(txtName);
		
		this.acceptButton.setLabel("Ajouter");
		
		this.pack();
	}

	@Override
	protected void onAccept() {
		// TODO Auto-generated method stub
		ctl.addNewSubscriber(txtName.getText());
	}
}
