package view;

import java.awt.Choice;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import utility.Arranger;
import controler.TransportControler;

public class BadgingDialog extends MyAbstractDialog {
	private static final long serialVersionUID = 1L;
	protected Choice subscriber  = new Choice();
	protected Choice currentStop = new Choice();
	protected Choice targetStop  = new Choice();
	protected Choice vehicleType = new Choice();
	protected Arranger<String> subscriberList;
 	protected Arranger<String> targetStopList;
	protected Arranger<String> vehicleTypeList;  
 
	public BadgingDialog(TransportControler ctl, Frame owner) {
		super(ctl, "Badger", owner);
		initialize(ctl, owner);
	}
	
	public BadgingDialog(TransportControler ctl, Frame owner, int x, int y, int currentStopIndex) {
		super(ctl, "Badger", owner);
		initialize(ctl, owner);
		setLocation(x, y);
		setCurrentStop(currentStopIndex);
	}
	
	private void initialize(TransportControler ctl, Frame owner) {
		this.setLocation(500, 300);
		subscriberList = ctl.getSubscribersNotInAnyVehicle();
		for (String s : subscriberList.getObjects()) this.subscriber.add(s);
		for(String s : TransportControler.getAllStopsNames()) {
			this.currentStop.add(s);
			this.targetStop.add(s);
		}
		refreshTargetStopList();
				
		subscriber.addItemListener(new MyItemListener());
		currentStop.addItemListener(new MyItemListener());
		targetStop.addItemListener(new MyItemListener());
		vehicleType.addItemListener(new MyItemListener());
		
		Panel panel1 = new Panel();
		panel1.setLayout(new GridLayout(2, 4));
		
		panel1.add(new Label("Nom de l'abonn�"));
		panel1.add(new Label("Arr�t de d�part"));
		panel1.add(new Label("Arr�t de d�stination"));
		panel1.add(new Label("Type de v�hicule"));
		panel1.add(subscriber);
		panel1.add(currentStop);
		panel1.add(targetStop);
		panel1.add(vehicleType);
		
		this.center.add(panel1);
		
		vehicleType.add("             ");
		
		this.acceptButton.setLabel("Badger");
		
		this.pack();
	}
	
	public void setCurrentStop(int currentStopIndex) {
		currentStop.select(currentStopIndex);
		refreshTargetStopList();
	}
	
	private void refreshTargetStopList() {
		targetStop.removeAll();
		targetStopList = TransportControler.getNamesOfLinkedStopsWithIndexedStop(currentStop.getSelectedIndex());
		for (String s : targetStopList.getObjects()) {
			targetStop.add(s);
		}
		refreshVehicleTypeList();
	}
	
	private void refreshVehicleTypeList() {
		vehicleType.removeAll();
		vehicleTypeList = ctl.getIndexAndNamesOfAvailableVehicleTypes(currentStop.getSelectedIndex(), targetStopList.getValue((targetStop.getSelectedIndex())));
		for (String s : vehicleTypeList.getObjects()) {
			vehicleType.add(s);
		}
	}
	
	protected class MyItemListener implements ItemListener {
		@Override
		public void itemStateChanged(ItemEvent e) {
			if (e.getSource() == currentStop) {
				refreshTargetStopList();
			}
			else if (e.getSource() == targetStop) {
				refreshVehicleTypeList();
			}
		}
	}

	@Override
	protected void onAccept() {
		ctl.badge(
			subscriberList.getValue(subscriber.getSelectedIndex()), 
			currentStop.getSelectedIndex(), 
			targetStopList.getValue(targetStop.getSelectedIndex()),
			vehicleTypeList.getValue(vehicleType.getSelectedIndex())
		);
	}
}
