package view;

import java.awt.Choice;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import utility.Arranger;
import controler.TransportControler;

public class ChangingStopDialog extends MyAbstractDialog {
	private static final long serialVersionUID = 1L;
	protected Choice subscriber = new Choice();
	protected Choice targetStop = new Choice();
	protected Arranger<String> subscriberList;
 	protected Arranger<String> targetStopList;
 
	public ChangingStopDialog(TransportControler ctl, Frame owner) {
		super(ctl, "Faire sortir", owner);
		initialize(ctl, owner);
	}
	
	private void initialize(TransportControler ctl, Frame owner) {
		this.setLocation(500, 300);
		subscriberList = ctl.getSubscribersInVehicles();
		for (String s : subscriberList.getObjects()) this.subscriber.add(s);

		refreshTargetStopList();
				
		subscriber.addItemListener(new MyItemListener());
		targetStop.addItemListener(new MyItemListener());
		
		Panel panel1 = new Panel();
		panel1.setLayout(new GridLayout(2, 2));
		
		panel1.add(new Label("Nom de l'abonn�"));
		panel1.add(new Label("Arr�t de d�stination"));
		panel1.add(subscriber);
		panel1.add(targetStop);
		
		this.center.add(panel1);
		
		
		this.acceptButton.setLabel("Valider");
		
		this.pack();
	}
	
	
	private void refreshTargetStopList() {
		targetStop.removeAll();
		targetStopList = ctl.getStopsBetweenSubscriberAndTarget(subscriberList.getValue(subscriber.getSelectedIndex()));
		for (String s : targetStopList.getObjects()) {
			targetStop.add(s);
		}
	}
	
	
	protected class MyItemListener implements ItemListener {
		@Override
		public void itemStateChanged(ItemEvent e) {
			if (e.getSource() == subscriber) {
				refreshTargetStopList();
			}
		}
	}

	@Override
	protected void onAccept() {
		ctl.setSubscriberTargetStopIndex(
			subscriberList.getValue(subscriber.getSelectedIndex()), 
			targetStopList.getValue(targetStop.getSelectedIndex())
		);
	}
}
