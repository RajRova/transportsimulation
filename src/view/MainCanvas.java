package view;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

public class MainCanvas extends Canvas {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BufferedImage bufferedImage;
	protected Graphics buf;

	MainCanvas() {
		this.setSize(800, 600);
		//this.setBackground(Color.green);
		this.setVisible(true);
		bufferedImage = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
		buf = bufferedImage.getGraphics();
	}
	
	public void drawBackGround() {
		buf.setColor(new Color(190, 190, 190));
		buf.fillRect(0, 0, getWidth(), getHeight());
	}
	
	public synchronized void paint(Graphics g) {
		g.drawImage(bufferedImage, 0, 0, this);
	}
	
	public void update(Graphics g) {
		paint(g);
	}
	
	public Graphics getBuffer() {
		return buf;
	}
	
	public void drawImageToBuffer(Image img, int x, int y) {
		buf.drawImage(img, x, y, this);
	}	
	
	public void drawImageToBuffer(Image img, int x, int y, int w, int h) {
		buf.drawImage(img, x, y, w, h, this);
	}
}
