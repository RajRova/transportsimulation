package view;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import utility.Drawable;
import controler.TransportControler;

public class MainFrame extends Frame implements ActionListener {
	private static final long serialVersionUID = 1L;
	public ArrayList<Drawable> locatedObjects = new ArrayList<Drawable>();
	public ArrayList<Drawable> stops = new ArrayList<Drawable>();
	public ArrayList<Drawable> vehicles = new ArrayList<Drawable>();
	public ArrayList<Drawable> paths = new ArrayList<Drawable>();
//	public ArrayList<>

	protected MainCanvas mainCanvas;
	protected Panel pCenter = new Panel();
	protected Panel pNorth =  new Panel();
	protected Panel pSouth =  new Panel();
	protected Button bAddSubscriber = new Button("Ajouter abonn�");
	protected Button bBadge         = new Button("Faire badger");
	protected Button bChangeStop    = new Button("Faire descendre");
	protected Button bAccelerate    = new Button("Accelerer");
	protected Button bDecelerate    = new Button("Ralentir");
	protected Button bStartStop     = new Button("Pause");
	protected TransportControler ctl;
	protected boolean suspended = false;
	
	public MainFrame(TransportControler ctl) {
		super("Simulation de r�seau de transport");
		
		this.ctl = ctl;

		this.setLayout(new BorderLayout());
		
		pNorth.setLayout(new FlowLayout());

		mainCanvas = new MainCanvas();
		
		mainCanvas.setLocation(this.getInsets().left+10, this.getInsets().top+32);
		this.addWindowListener(new MyWinListener());
		
		bAddSubscriber.addActionListener(this);
		bBadge.addActionListener(this);
		bBadge.setEnabled(false);
		bChangeStop.setEnabled(false);
		bChangeStop.addActionListener(this);
		bAccelerate.addActionListener(this);
		bDecelerate.addActionListener(this);
		bStartStop.addActionListener(this);
		
		pCenter.add(mainCanvas);
		pNorth.add(bAddSubscriber);
		pNorth.add(bBadge);
		pNorth.add(bChangeStop);
		pSouth.add(bStartStop);
		pSouth.add(bDecelerate);
		pSouth.add(bAccelerate);

		
		this.add(BorderLayout.CENTER, pCenter);
		this.add(BorderLayout.NORTH, pNorth);
		this.add(BorderLayout.SOUTH, pSouth);
		this.pack();
		this.setLocation(10, 10);
		this.setResizable(false);
		//this.setSize(800, 600);
	}
	
	public void addLocatedObject(Drawable lo) {
		locatedObjects.add(lo);
	}
	
	public void addVehicle(Drawable lo) {
		vehicles.add(lo);
	}
	
	public void addStop(Drawable lo) {
		stops.add(lo);
	}
	
	public void addPath(Drawable path) {
		paths.add(path);
	}
	
	public void paint(Graphics g) {
		mainCanvas.drawBackGround();
		
		for (Drawable p : paths) {
			drawLocatedObject(p);
		}

		for (Drawable s : stops) {
			drawLocatedObject(s);
		}

		for (Drawable v : vehicles) {
			drawLocatedObject(v);
		}

		
		for (Drawable lo : locatedObjects) {
			drawLocatedObject(lo);
		}
		
		//repaint();

		mainCanvas.repaint();
	}
	

	public void drawLocatedObject(Drawable lo) {
		lo.draw(mainCanvas.getBuffer());
	}
	
	public boolean mouseDown(Event e, int x, int y) {
		mainCanvas.repaint();

		if (bBadge.isEnabled()) {
			for (Drawable s : stops) {
				if (s.imageContainsPoint(x - mainCanvas.getX(), y - mainCanvas.getY() - 60)) {
					ctl.showBadgingDialog(x, y, stops.indexOf(s));
					break;
				}
			}
		}
			
		return true;
	}
	
	public void setEnabledBadging(boolean b) {
		this.bBadge.setEnabled(b);
	}
	
	public void setEnabledChangeStop(boolean b) {
		bChangeStop.setEnabled(b);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == bAddSubscriber) {
			AddSubscriberDialog f = new AddSubscriberDialog(ctl, this);
			f.setVisible(true);
		}
		else if (arg0.getSource() == bBadge) {
			ctl.showBadgingDialog();
		}
		else if (arg0.getSource() == bChangeStop) {
			ctl.showChangingStopDialog();
		}
		else if (arg0.getSource() == bAccelerate) {
			ctl.decreasePeriod();
		}
		else if (arg0.getSource() == bDecelerate) {
			ctl.increasePeriod();
		}
		else if (arg0.getSource() == bStartStop) {
			if (suspended) {
				bStartStop.setLabel("Pause");
				ctl.resume();
			}
			else {
				bStartStop.setLabel("Relancer");
				ctl.suspend();
			}
			suspended = !suspended;
		}
	}
}

