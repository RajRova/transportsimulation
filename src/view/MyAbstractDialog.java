package view;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import controler.TransportControler;

public abstract class MyAbstractDialog extends Dialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	protected Panel center = new Panel();
	protected Panel south = new Panel();
	protected Button acceptButton = new Button("Accepter");
	protected Button cancelButton = new Button("Annuler");
	
	protected TransportControler ctl;
	
	public MyAbstractDialog(TransportControler ctl, String title, Frame owner) {
		super(owner, title, true);
		this.ctl = ctl;
		
		this.setLayout(new BorderLayout());
		
		center.setLayout(new FlowLayout());
		
		south.setLayout(new FlowLayout());
		south.add(cancelButton);
		south.add(acceptButton);
		
		acceptButton.addActionListener(this);
		cancelButton.addActionListener(this);
		
		this.add(BorderLayout.CENTER, center);
		this.add(BorderLayout.SOUTH, south);
		
		this.setResizable(false);
		
		this.addWindowListener(new MyWinListener(this));
		this.pack();
	}
	
	protected void onCancel() {
	}
	
	protected abstract void onAccept();

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if (arg0.getSource() == this.cancelButton) {
			onCancel();
			this.dispose();
		}
		else if (arg0.getSource() == this.acceptButton) {
			onAccept();
			this.dispose();
		}
	}
}
