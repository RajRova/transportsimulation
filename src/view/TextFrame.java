package view;

import java.awt.Frame;
import java.awt.TextArea;

public class TextFrame extends Frame {
	private static final long serialVersionUID = 1L;
	protected TextArea textArea = new TextArea();
	
	public TextFrame () {
		this.add(textArea);
		this.setLocation(850, 10);
		this.setSize(500, 700);
	}
	
	public void println(String s) {
		textArea.append(s + "\n");
	}
	
	public void addText(String s) {
		textArea.append(s);
	}
}
